module gitlab.com/lamados/random

go 1.18

require (
	github.com/lucasb-eyer/go-colorful v1.2.0
	gonum.org/v1/gonum v0.11.0
)

require golang.org/x/exp v0.0.0-20220722155223-a9213eeb770e // indirect
