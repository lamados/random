package random

import (
	"strconv"
	"testing"
)

func BenchmarkWordGenerator(b *testing.B) {
	gen := NewWordGenerator()
	max := 8

	for i := 0; i <= max; i++ {
		n := (1 << i) - 1
		b.Run(strconv.Itoa(n), func(b *testing.B) {
			for j := 0; j < b.N; j++ {
				_ = gen.NewWord(n)
			}
		})
	}
}

func BenchmarkWordGenerator_Parallel(b *testing.B) {
	gen := NewWordGenerator()
	maxBias := 8

	for i := 0; i <= maxBias; i++ {
		n := (1 << i) - 1
		b.Run(strconv.Itoa(n), func(b *testing.B) {
			b.RunParallel(func(pb *testing.PB) {
				for pb.Next() {
					_ = gen.NewWord(n)
				}
			})
		})
	}
}
