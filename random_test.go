package random

import (
	"math"
	"sort"
	"strconv"
	"testing"
	"time"
)

func TestDuration(t *testing.T) {
	lower := -8 * time.Minute
	upper := 8 * time.Minute

	Duration(lower, upper)
}

func TestFloat(t *testing.T) {
	lower := -8.0
	upper := 8.0

	Float(lower, upper)
}

func TestInt(t *testing.T) {
	lower := -8
	upper := 8

	Int(lower, upper)
}

func BenchmarkWeightedElement(b *testing.B) {
	max := 6

	for i := 1; i <= max; i++ {
		n := int(math.Pow10(i))
		b.Run(strconv.Itoa(n), func(b *testing.B) {
			var choices []Choice[string]
			for i := 0; i < n; i++ {
				choices = append(choices, Choice[string]{Int(1, 10), Word()})
			}

			b.ResetTimer()

			for i := 0; i < b.N; i++ {
				_ = WeightedElement(choices)
			}
		})
	}
}

func BenchmarkWeightedElement_PreSorted(b *testing.B) {
	max := 6

	for i := 1; i <= max; i++ {
		n := int(math.Pow10(i))
		b.Run(strconv.Itoa(n), func(b *testing.B) {
			var choices []Choice[string]
			for i := 0; i < n; i++ {
				choices = append(choices, Choice[string]{Int(1, 10), Word()})
			}

			sort.Slice(choices, func(i, j int) bool {
				return choices[i].Weight > choices[j].Weight
			})

			b.ResetTimer()

			for i := 0; i < b.N; i++ {
				_ = WeightedElement(choices)
			}
		})
	}
}
