// Package random implements a variety of functions for generating random things which may or may not be useful.
//
// Functions in here use the regular pseudo-random number generation found in "math/rand", and so shouldn't be assumed
// to be cryptographically safe unless otherwise specified. They also use the default RNG source and so may need to be
// seeded manually using rand.Seed.
package random

import (
	cryptorand "crypto/rand"
	mathrand "math/rand"
	"time"

	"github.com/lucasb-eyer/go-colorful"
)

// Int generates a random int with the given lower and upper bounds.
//
// The lower and upper bounds are inclusive, so lower is the minimum number it could return and upper is the maximum
// number it could return.
func Int(lower, upper int) int {
	if lower > upper {
		lower, upper = upper, lower
	}

	return lower + mathrand.Intn((upper+1)-lower)
}

// Float generates a random float with the given lower and upper bounds.
func Float(lower, upper float64) float64 {
	return lower + mathrand.Float64()*(upper-lower)
}

// Duration generates a random duration with the given lower and upper bounds.
func Duration(lower, upper time.Duration) time.Duration {
	if lower > upper {
		lower, upper = upper, lower
	}

	return lower + time.Duration(mathrand.Int63n(int64(upper)-int64(lower)))
}

// Bool generates a random bool.
func Bool() bool {
	return mathrand.Intn(2) == 0
}

// Element returns a random element from a slice.
func Element[T any](slice []T) T {
	return slice[mathrand.Intn(len(slice))]
}

// Choice is a weighted value for use in WeightedElement.
type Choice[T any] struct {
	Weight int
	Value  T
}

// WeightedElement returns a random element from a slice of weighted choices.
func WeightedElement[T any](choices []Choice[T]) T {
	total := 0
	for _, choice := range choices {
		total += choice.Weight
	}

	if total == 0 {
		var t T
		return t
	}

	x := mathrand.Intn(total)
	for _, choice := range choices {
		x -= choice.Weight
		if x < 0 {
			return choice.Value
		}
	}

	panic("unreachable")
}

// Bytes generates a cryptographically-safe slice of bytes with the given length.
func Bytes(length int) []byte {
	raw := make([]byte, length)

	_, err := cryptorand.Read(raw)
	if err != nil {
		panic(err)
	}

	return raw
}

// Color returns a random color, with 100% saturation and lightness.
func Color() colorful.HexColor {
	return colorful.HexColor(colorful.Hsl(mathrand.Float64()*360, 1, 1))
}

// Colour is an alias for Color.
func Colour() colorful.HexColor { return Color() }
