package random

import (
	"math/rand"
	"strings"
	"sync"

	"gonum.org/v1/gonum/graph"
	"gonum.org/v1/gonum/graph/multi"
)

var (
	consonants = []string{"b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "q", "r", "s", "t", "v", "w", "x", "y", "z"}
	compounds  = []string{"ch", "sh", "ll", "ts", "ph", "th", "ss", "fl", "ff", "ck", "nt", "nk", "ty", "tch", "qu", "st", "sc", "ng", "gh", "rh", "wh", "wr", "tl", "sk", "cr", "kr", "fr", "pl", "ft", "ls", "ct", "ld", "tt", "pp", "rr", "mm", "nn", "dd", "cc"}
	vowels     = []string{"a", "e", "i", "o", "u"}

	startings = []string{"ch", "sh", "sch", "ts", "ph", "th", "fl", "qu", "st", "sc", "rh", "wh", "wr", "sk", "dr", "tr", "gr", "pr", "cr", "kr", "fr", "pl", "cl"}
	endings   = []string{"ch", "sh", "sch", "ts", "ph", "th", "ck", "nt", "nk", "ty", "ld", "tch", "st", "ng", "gh", "sk", "rd", "rt", "rg", "rp", "lc", "mn", "mb", "ct", "sc"}
)

var gen *WordGenerator
var genInit sync.Once

// Word generates a randomised and (hopefully) semi-readable word, approximately 8 characters in length.
//
// This is a wrapper around *WordGenerator.NewWord using a bias of 4. If longer or shorter words are required, use
// NewWordGenerator to create your own word generator.
//
// Word will take longer on first run, since it doesn't initialise the word generator until it's used.
func Word() string {
	genInit.Do(func() {
		gen = NewWordGenerator()
	})

	return gen.NewWord(4)
}

// WordGenerator is a random word generator.
type WordGenerator struct {
	startingNodes []graph.Node
	graph         *multi.DirectedGraph
	ids           map[string]int64
}

// NewWordGenerator creates a new word generator, setting up the internal graph used to string characters together.
func NewWordGenerator() *WordGenerator {
	// create maps for tracking nodes.
	nodes := make(map[string]*node)
	startingNodes := make(map[*node]struct{})
	endingNodes := make(map[*node]struct{})
	consonantNodes := make(map[*node]struct{})
	compoundNodes := make(map[*node]struct{})
	vowelNodes := make(map[*node]struct{})
	ids := make(map[string]int64)

	// set up ID counter.
	i := int64(0)

	// set up starting nodes.
	for _, s := range startings {
		n, ok := nodes[s]
		if !ok {
			n = &node{
				id: i,
				s:  s,
			}
			ids[s] = n.id
			nodes[s] = n
			i++
		}

		n.Starting = true
		startingNodes[n] = struct{}{}
	}

	// set up ending nodes.
	for _, s := range endings {
		n, ok := nodes[s]
		if !ok {
			n = &node{
				id: i,
				s:  s,
			}
			ids[s] = n.id
			nodes[s] = n
			i++
		}

		n.Ending = true
		endingNodes[n] = struct{}{}
	}

	// set up compound nodes.
	for _, s := range compounds {
		n, ok := nodes[s]
		if !ok {
			n = &node{
				id: i,
				s:  s,
			}
			ids[s] = n.id
			nodes[s] = n
			i++
		}

		n.Compound = true
		compoundNodes[n] = struct{}{}
	}

	// set up consonant nodes.
	for _, s := range consonants {
		n, ok := nodes[s]
		if !ok {
			n = &node{
				id: i,
				s:  s,
			}
			ids[s] = n.id
			nodes[s] = n
			i++
		}

		n.Consonant = true
		n.Ending = true
		consonantNodes[n] = struct{}{}
		endingNodes[n] = struct{}{}
	}

	// set up vowel nodes.
	for _, s := range vowels {
		n, ok := nodes[s]
		if !ok {
			n = &node{
				id: i,
				s:  s,
			}
			ids[s] = n.id
			nodes[s] = n
			i++
		}

		n.Vowel = true
		n.Starting = true
		n.Ending = true
		vowelNodes[n] = struct{}{}
		startingNodes[n] = struct{}{}
		endingNodes[n] = struct{}{}
	}

	// create graph.
	g := multi.NewDirectedGraph()

	// create edges on graph.
	for _, from := range nodes {
		// add edges to all vowel nodes.
		for to := range vowelNodes {
			g.SetLine(g.NewLine(from, to))
		}

		if from.Vowel {
			// add edges to all ending nodes.
			for to := range endingNodes {
				if from != to {
					g.SetLine(g.NewLine(from, to))
				}
			}

			// add edges to all compound nodes.
			for to := range compoundNodes {
				if from != to {
					g.SetLine(g.NewLine(from, to))
				}
			}

			// add edges to all consonant nodes.
			for to := range consonantNodes {
				if from != to {
					g.SetLine(g.NewLine(from, to))
				}
			}
		}
	}

	// add all starting nodes to a slice.
	ns := make([]graph.Node, 0, len(startingNodes))
	for n := range startingNodes {
		ns = append(ns, n)
	}

	// return new word generator.
	return &WordGenerator{startingNodes: ns, graph: g, ids: ids}
}

// NewWord generates a randomised and (hopefully) semi-readable word.
//
// The given bias is used to set the length of the word. This isn't measured in characters and just influences how many
// iterations it takes before the word generator stops and returns. In general, a word  will be approximately (bias*2)±1
// characters long. 2 to 5 is usually the sweet spot.
//
// Note that there is no filtering done for profanity or offensive words, and so there is a non-zero chance that this
// method will return something horrible. Filtering should be done separately if this is desired, and I apologise in
// advance.
func (gen *WordGenerator) NewWord(bias int) string {
	b := strings.Builder{}

	// select random starting node.
	n := Element(gen.startingNodes).(*node)

	for {
		bias--

		// write node string to builder.
		b.WriteString(n.s)

		// exit loop if ending node.
		if n.Ending && bias < 0 {
			break
		}

		// exit loop if bias is too low.
		if bias < 0 && !n.Compound && !n.Starting {
			break
		}

		// get a random node.
		ns := gen.graph.From(n.id)
		for i := rand.Intn(ns.Len()); i >= 0; i-- {
			ns.Next()
		}
		n = ns.Node().(*node)
	}

	// return word.
	return b.String()
}

// CanGenerate returns true if the word generator is able to generate the given string.
//
// Only lowercase strings with alphabetical characters are supported here.
func (gen *WordGenerator) CanGenerate(s string) bool {
	l := len(s)
	i, j := 0, l
	var n graph.Node

	// walk over the given string.
	for {
		if i == j {
			return false
		}

		// see if node exists for this string.
		id, ok := gen.ids[s[i:j]]
		if !ok {
			j--
			continue
		}

		// return true if come to end of string.
		if j == l {
			return true
		}

		// if this is first run, set node and continue on.
		if n == nil {
			// set node.
			n = gen.graph.Node(id)

			// check if this is a valid starting node.
			nd := n.(*node)
			if !nd.Starting && !nd.Vowel && !nd.Consonant {
				return false
			}

			i = j
			j = l
			continue
		}

		// see if there's a connecting line between the last node and this one.
		if !gen.graph.HasEdgeFromTo(n.ID(), id) {
			j--
			continue
		}

		// set new bounds for string.
		i = j
		j = l

		// set node.
		n = gen.graph.Node(id)
	}
}

type node struct {
	id int64
	s  string

	Starting  bool
	Ending    bool
	Consonant bool
	Compound  bool
	Vowel     bool
}

func (n *node) ID() int64 {
	return n.id
}
